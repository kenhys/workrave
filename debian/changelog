workrave (1.10.44-1) unstable; urgency=medium

  * New upstream release
  * Switch build dependency to libayatana-indicator3 (closes: #956759).

 -- Francois Marier <francois@debian.org>  Tue, 12 May 2020 20:42:24 -0700

workrave (1.10.43-1) unstable; urgency=medium

  * New upstream release

 -- Francois Marier <francois@debian.org>  Mon, 13 Apr 2020 17:47:12 -0700

workrave (1.10.42-1) unstable; urgency=medium

  * New upstream release.

 -- Francois Marier <francois@debian.org>  Fri, 10 Apr 2020 10:38:54 -0700

workrave (1.10.37-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version up to 4.5.0.
  * Switch from debian/compat to debhelper-compat (= 12).
  * Build without root.

 -- Francois Marier <francois@debian.org>  Wed, 18 Mar 2020 21:54:07 -0700

workrave (1.10.34-3) unstable; urgency=medium

  * Remove python-cheetah build dependency. (closes: #939480)

 -- Francois Marier <francois@debian.org>  Thu, 12 Sep 2019 09:17:56 -0700

workrave (1.10.34-2) unstable; urgency=medium

  * Add missing build-dep on libboost-dev and python3-jinja2
  * Bump Standards-Version up to 4.4.0

 -- Francois Marier <francois@debian.org>  Sun, 07 Jul 2019 13:36:55 -0700

workrave (1.10.34-1) unstable; urgency=medium

  * New upstream release
  * Fix workrave-dump script (closes: #826021)
  * Add metadata license to debian/copyright
  * Remove patches applied upstream
    - fix_appstream_xml.patch
    - fix_location_of_appdata.patch

 -- Francois Marier <francois@debian.org>  Sat, 06 Jul 2019 13:46:17 -0700

workrave (1.10.23-5) unstable; urgency=medium

  * Bump Standards-Version up to 4.3.0
  * Bump debhelper compat up to 12
  * Run wrap_and_sort -ast

 -- Francois Marier <francois@debian.org>  Fri, 01 Mar 2019 23:43:21 -0800

workrave (1.10.23-2) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Use debian/workrave.docs to install NEWS
  * Drop --with autoreconf; it's the default since dh compat 10
  * Switch to dh_missing --fail-missing
  * Drop unused debian/workrave.lintian-overrides
  * Don't build panel applet on s390x
    - The required library is being removed there (#906016)
    - Build-depend on dh-exec and update debian/*.install files
  * Handle Ubuntu's indicator directory install location

 -- Francois Marier <francois@debian.org>  Tue, 16 Oct 2018 18:10:38 -0700

workrave (1.10.23-1) unstable; urgency=medium

  * New upstream version

 -- Francois Marier <francois@debian.org>  Mon, 01 Oct 2018 22:53:00 -0700

workrave (1.10.21-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix AppStream metadata format
  * Fix location of AppStream metadata
  * Lintian override for package naming

  [ Francois Marier ]
  * New upstream version
  * Bump Standards-Version up to 4.2.1

 -- Francois Marier <francois@debian.org>  Sat, 08 Sep 2018 11:16:28 -0700

workrave (1.10.20-1) unstable; urgency=medium

  * New upstream version
    - drop all debian patches (applied upstream)
    - stop installing the renamed README since it's unnecessary
  * Switch VCS to salsa.debian.org
  * Switch to priority: optional in debian/control

  * Bump Standards-Version to 4.1.3
  * Bump debhelper compat to 11 and remove unnecessary build-deps
  * Remove trailing whitespace from debian/changelog (lintian notice)
  * Use https version of copyright format URI (lintian notice)
  * Update copyright years in debian/copyright

 -- Francois Marier <francois@debian.org>  Sun, 11 Feb 2018 17:03:38 -0800

workrave (1.10.16-2) unstable; urgency=medium

  * Add panel_applet_inprocess.patch from upstream (closes: #866386)
  * Update the gnome-panel module location for gnome-panel 3.22
  * Bump Standards-Version to 4.0.0

 -- Francois Marier <francois@debian.org>  Mon, 03 Jul 2017 21:45:40 -0700

workrave (1.10.16-1) unstable; urgency=medium

  * New upstream release
  * Update gnome_322 patch

 -- Francois Marier <francois@debian.org>  Sat, 17 Sep 2016 12:53:16 -0700

workrave (1.10.15-4) unstable; urgency=medium

  * Mark applet as compatible with GNOME 3.21/3.22 (closes: #837214)

 -- Francois Marier <francois@debian.org>  Sun, 11 Sep 2016 18:56:37 -0700

workrave (1.10.15-3) unstable; urgency=medium

  * Fix FTBFS with dpkg-buildpackage -A (closes: #830852)

 -- Francois Marier <francois@debian.org>  Wed, 13 Jul 2016 21:55:51 -0700

workrave (1.10.15-2) unstable; urgency=medium

  * Ship contrib graphing tools in doc/workrave/examples (closes: #825982)

 -- Francois Marier <francois@debian.org>  Sun, 03 Jul 2016 15:09:06 -0700

workrave (1.10.15-1) unstable; urgency=medium

  * New upstream release

 -- Francois Marier <francois@debian.org>  Wed, 11 May 2016 20:42:57 -0700

workrave (1.10.14-1) unstable; urgency=medium

  * New upstream release

 -- Francois Marier <francois@debian.org>  Sat, 07 May 2016 14:55:10 -0700

workrave (1.10.13-1) unstable; urgency=medium

  * New upstream release
    - drop gnome-shell_320.patch (applied upstream)

 -- Francois Marier <francois@debian.org>  Wed, 27 Apr 2016 23:51:43 -0700

workrave (1.10.12-2) unstable; urgency=medium

  * Make applet compatible with gnome-shell 3.20 (closes: #822373)
  * Fix path of libindicators files in workrave.install (closes: #822402)
  * Add missing mate and cinammon files to the package
  * Bump Standards-Version to 3.9.8

 -- Francois Marier <francois@debian.org>  Tue, 26 Apr 2016 12:11:09 -0700

workrave (1.10.12-1) unstable; urgency=medium

  * New upstream release
    - drop gnome-shell-318.patch (applied upstream)
  * Move VCS-* fields to secure URLs
  * Bump Standards-Version to 3.9.7

 -- Francois Marier <francois@debian.org>  Tue, 29 Mar 2016 20:17:23 -0700

workrave (1.10.10-1) unstable; urgency=medium

  * New upstream release

 -- Francois Marier <francois@debian.org>  Sun, 13 Dec 2015 21:57:09 -0800

workrave (1.10.8-1) unstable; urgency=medium

  * New upstream release
  * Update gnome-shell patch
  * Add build-depend on autoconf-archive

 -- Francois Marier <francois@debian.org>  Wed, 11 Nov 2015 12:07:19 -0800

workrave (1.10.6-4) unstable; urgency=medium

  * Add build conflict on libgstreamer0.10-dev (closes: #785868)
  * Mark applet as compatible with gnome-shell 3.18 (closes: #802510)

 -- Francois Marier <francois@debian.org>  Thu, 22 Oct 2015 07:59:28 -0700

workrave (1.10.6-3) unstable; urgency=medium

  * Force c++11 in debian/rules (closes: #801651)
  * Enable all hardening compilation flags
  * Add support for the mate desktop environment (closes: #801077)
  * Remove legacy menu entry (lintian warning)

 -- Francois Marier <francois@debian.org>  Fri, 16 Oct 2015 09:34:12 -0700

workrave (1.10.6-2) unstable; urgency=medium

  * Drop versioned dependency on gnome-panel
  * Move xfce4-panel to Suggests (closes: #783845)
  * Run wrap-and-sort
  * Switch the build-dependency from libpanel-applet-4-dev to
    libpanel-applet-dev (Ubuntu merge)
  * Switch build-dependency from libgstreamer0.10-dev to
    libgstreamer1.0-dev (closes: #785868)

 -- Francois Marier <francois@debian.org>  Mon, 22 Jun 2015 10:47:58 -0700

workrave (1.10.6-1) unstable; urgency=medium

  * New upstream release
    - drop GNOME 3.14 patch which was applied upstream
  * Enable XFCE Panel plugin (closes: #677469)
  * Disable upstream tests since they no longer work

 -- Francois Marier <francois@debian.org>  Sat, 25 Apr 2015 21:50:44 +1200

workrave (1.10.4-3) unstable; urgency=medium

  * Mark the applet as compatible with GNOME 3.14 (closes: #762727)

 -- Francois Marier <francois@debian.org>  Sat, 04 Oct 2014 17:04:29 +1300

workrave (1.10.4-2) unstable; urgency=medium

  * Bump Standards-Version up to 3.9.6

 -- Francois Marier <francois@debian.org>  Fri, 19 Sep 2014 00:45:03 +1200

workrave (1.10.4-1) unstable; urgency=medium

  * New upstream release (closes: #754908)
  * Drop all Debian patches
  * Build with autoreconf (makefiles are not in the github tarballs)
  * Point watch file to github releases (closes: #755101)

 -- Francois Marier <francois@debian.org>  Wed, 23 Jul 2014 11:58:16 +1200

workrave (1.10.1-4) unstable; urgency=medium

  * Revert the Ubuntu changes from 1.10.1-3 (closes: #737034)
  * Manual libtool update to fix FTBFS on ppc64el

 -- Francois Marier <francois@debian.org>  Sat, 01 Feb 2014 17:25:10 +1300

workrave (1.10.1-3) unstable; urgency=medium

  * Merge Ubuntu changes (closes: #736694)
    - Use dh-autoreconf to get new libtool macros for ppc64el
    - Disable -Werror to fix FTBFS while autoreconfing
    - Remove gettext macros to ensure libtool is creating po/Makefile.in.in
  * Bump Standards-Version up to 3.9.5

 -- Francois Marier <francois@debian.org>  Sun, 26 Jan 2014 19:09:08 +1300

workrave (1.10.1-2) unstable; urgency=low

  [ Sébastien Villemot ]
  * gnome_3.8.patch: makes the GNOME Shell extension compatible with GNOME 3.8
    (Closes: #720037)

  [ Francois Marier ]
  * Merge with Ubuntu:
    - link against libindicator3-dev and build the indicator

 -- Francois Marier <francois@debian.org>  Thu, 05 Sep 2013 09:06:56 +1200

workrave (1.10.1-1) unstable; urgency=low

  * New upstream version
  * Merge with Ubuntu:
    - fix errors in the desktop file for Polish and Russian locales

  * Update debian/copyright for upstream move to GPLv3+
  * Remove unused intltool_vs_gettextize.patch
  * Bump Standards-Version up to 3.9.4
  * Use canonical VCS URLs in debian/control

 -- Francois Marier <francois@debian.org>  Sat, 15 Jun 2013 12:56:51 +1200

workrave (1.9.909+abc941eb70-1) unstable; urgency=low

  [ Francois Marier ]
  * New upstream snapshot
    - Drop leak-fix patch (applied upstream)
    - Document how the tarball is built in README.source
  * Build GNOME applets and use gsettings
  * Massive update of Build-Depends as per configure.ac

  * Update README.source with snapshot instructions
  * Switch to machine-readable copyright file
  * Update alioth git repo links
  * Bump debhelper version to 9
  * Bump Standards-Version to 3.9.3

  [ Jordi Mallach ]
  * Avoid references to GNU/Linux in manpage.
  * Drop build dependency on libgnet-dev, it's obsolete and unneeded.
  * Add myself to Uploaders.
  * Rewrite d/rules into dh style.
    - Move all install tweaks to .install files.
    - Install manpages using dh_installman.
  * As a side effect, the package installs arch-dependant data in the
    arch triplet directory; add the required Pre-Depends for m-a-support.
  * Bring back GNOME Panel applet (for GNOME 3 fallback mode) and ship the
    new GNOME Shell extension (closes: #642514, #666100).
  * Add private_dirs.patch: move libworkrave-private and GObject
    Introspection files to a private dir, so they are really out of the
    way, but disable it for now as it breaks the Shell extension.
  * Move typelib out of the triplet dir as gobject-introspection is not
    M-A ready yet.
  * Enable dh_autoreconf for the above patches.
  * Add lintian overrides.
  * Add necessary Breaks/Replaces as the xpm icon has moved to workrave-data.
  * Prefix all debhelper files with package name.
  * Suggest gnome-shell and gnome-panel.

 -- Francois Marier <francois@debian.org>  Mon, 28 May 2012 11:29:40 +1200

workrave (1.9.4-3) unstable; urgency=low

  * Apply upstream patch to fix memory leak (closes: #648597)

 -- Francois Marier <francois@debian.org>  Mon, 14 Nov 2011 11:21:59 +1300

workrave (1.9.4-2) unstable; urgency=low

  * Stop building GNOME application and applet (closes: #638100, #614038)

  * Bump Standards-Version to 3.9.2
  * Add empty build-arch and build-indep target in debian/rules (lintian)

 -- Francois Marier <francois@debian.org>  Sun, 11 Sep 2011 22:14:48 +1200

workrave (1.9.4-1) unstable; urgency=low

  * New upstream release
    - drop all Debian patches (applied upstream)

  * Update location of .xpm menu icon
  * Remove empty /usr/include directory after building
  * Remove obsolete breaks/replaces from debian/control

 -- Francois Marier <francois@debian.org>  Fri, 25 Mar 2011 17:17:18 +1300

workrave (1.9.3-2) unstable; urgency=medium

  * Add missing build dependency on libpulse-dev (closes: #610275)

 -- Francois Marier <francois@debian.org>  Mon, 17 Jan 2011 13:05:21 +1300

workrave (1.9.3-1) unstable; urgency=low

  * New upstream release (closes: #598222)
    - detects concurrent instances (closes: #266371)
    - drop gtkmm patch (applied upstream)
  * Backport 3 upstream commits for broken network connections
  * Patch a small typo in error messages (lintian notice)

  * Switch from Conflicts/Replaces to Breaks/Replaces in debian/control
  * Bump Standards-Version to 3.9.1
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Tue, 11 Jan 2011 20:59:08 +1300

workrave (1.9.1-3) unstable; urgency=medium

  * Sync with Ubuntu:
    - merge in gtkmm API changes (closes: #577317)

 -- Francois Marier <francois@debian.org>  Wed, 14 Apr 2010 12:04:25 +1200

workrave (1.9.1-2) unstable; urgency=low

  * Drop the dependency on libgnomeuimm-2.6-dev (closes: #568802)
  * Bump Standards-Version to 3.8.4
  * Switch to 3.0 (quilt) source format

 -- Francois Marier <francois@debian.org>  Tue, 09 Feb 2010 15:20:30 +1300

workrave (1.9.1-1) unstable; urgency=low

  * New upstream release (closes: #496842)
  * Drop all debian patches (merged upstream)

 -- Francois Marier <francois@debian.org>  Mon, 26 Oct 2009 13:57:43 +1300

workrave (1.9.0-8) unstable; urgency=low

  * Fix dpatch support so that patches are applied (closes: #550630)
  * Fix FTBFS on gcc 4.4 (closes: #550607, LP: 448742)

  * Bump Standards-Version to 3.8.3
  * Remove dh_desktop from debian/rules
  * Add README.source
  * Add a dependency on misc:depends (lintian warning)
  * Cleanup GPL notice (lintian notice)

 -- Francois Marier <francois@debian.org>  Mon, 12 Oct 2009 12:49:51 +1300

workrave (1.9.0-7) unstable; urgency=low

  * Fix timers on newer X servers (like Ubuntu Jaunty) using a patch from
    upstream (http://issues.workrave.org/cgi-bin/bugzilla/show_bug.cgi?id=804)
  * Bump Standards-Version to 3.8.1
  * Bump debhelper compatibility to 7
  * debian/rules: make use of dh_prep

 -- Francois Marier <francois@debian.org>  Sat, 21 Mar 2009 16:09:33 +1300

workrave (1.9.0-6) unstable; urgency=low

  * Changed the package priority to extra because of a dependency on
    libxcb-render-util0 which is also in the extra priority
  * Update watch file to accommodate dashes in the version number

 -- Francois Marier <francois@debian.org>  Tue, 11 Nov 2008 12:15:41 +1300

workrave (1.9.0-5) unstable; urgency=low

  * Fix the saving of the username value in the network preferences

 -- Francois Marier <francois@debian.org>  Wed, 10 Sep 2008 16:59:19 +1200

workrave (1.9.0-4) unstable; urgency=low

  * Add common/bin/DBus-glib.xml from upstream svn since it's missing
    from the tarball and appears to be required (closes: #497393)

 -- Francois Marier <francois@debian.org>  Thu, 04 Sep 2008 21:11:02 +1200

workrave (1.9.0-3) unstable; urgency=low

  * Add python-cheetah to Build-Depends (closes: #496331)

 -- Francois Marier <francois@debian.org>  Tue, 26 Aug 2008 17:24:36 +1200

workrave (1.9.0-2) unstable; urgency=low

  * Add intltool to Build-Depends (closes: #495031)
  * Fix watch file

 -- Francois Marier <francois@debian.org>  Thu, 14 Aug 2008 12:41:06 -0400

workrave (1.9.0-1) unstable; urgency=low

  * New upstream release (closes: #448860, #471261, #483532)
  * Add new build dependencies: libgdome2-dev and libgstreamer0.10-dev
  * Remove custom desktop and xpm files since they're now upstream

 -- Francois Marier <francois@debian.org>  Tue, 22 Jul 2008 09:55:02 +1200

workrave (1.8.5-7) unstable; urgency=low

  * Bump debhelper dependency/compatibility to 6
  * Bump Standards-Version to 3.8.0 (no changes)
  * Treat NEWS as the upstream changelog
  * Switch the packaging repo to git

 -- Francois Marier <francois@debian.org>  Sat, 12 Jul 2008 10:49:28 +1200

workrave (1.8.5-6) unstable; urgency=low

  * Add categories to the .desktop file (closes: #464527)
  * Proper copyright statement in debian/copyright
  * Create a new workrave-data package for arch-independent files
  * Remove RSI acronym from the short description

 -- Francois Marier <francois@debian.org>  Wed, 27 Feb 2008 15:10:06 +1300

workrave (1.8.5-5) unstable; urgency=low

  * Make debian/control point to the Workrave homepage, not the renameutils
    one (closes: #461779)
  * Fix GCC 4.3 compilation errors using a patch from Ubuntu (closes: #455113)
  * Remove Encoding key from the .desktop file
  * Fix spelling of GNOME in package description

 -- Francois Marier <francois@debian.org>  Sat, 26 Jan 2008 12:43:44 +1300

workrave (1.8.5-4) unstable; urgency=low

  * Fix the watch file (closes: #453544)
  * Bump Standards-Version to 3.7.3 (no changes)
  * Remove /usr/sbin from debian/dirs (it was empty)
  * Add a call to dh_desktop in debian/rules

 -- Francois Marier <francois@debian.org>  Tue, 11 Dec 2007 13:29:54 +1300

workrave (1.8.5-3) unstable; urgency=low

  * Rename XS-Vcs-* fields to Vcs-*
  * Sync with Ubuntu: fix the GNOME applet icon and category

 -- Francois Marier <francois@debian.org>  Wed, 21 Nov 2007 09:28:25 +1300

workrave (1.8.5-2) unstable; urgency=low

  * New feature: ability to disable the network server (and close that port),
    separately from disabling all network functionalities altogether
    (see http://issues.workrave.org/cgi-bin/bugzilla/show_bug.cgi?id=687)
  * Add homepage field to debian/control

 -- Francois Marier <francois@debian.org>  Thu, 25 Oct 2007 16:27:57 +1300

workrave (1.8.5-1) unstable; urgency=low

  * New upstream release (closes: #441930, #416252)
  * Add a menu icon for GNOME/KDE (closes: #318835)

 -- Francois Marier <francois@debian.org>  Tue, 25 Sep 2007 13:08:40 +1200

workrave (1.8.4-9) unstable; urgency=low

  * Fix display of portugese translation (closes: #440118)
    Thanks to Goedson Teixeira Paixao and Rob Caelers for the patch!

 -- Francois Marier <francois@debian.org>  Fri, 31 Aug 2007 12:51:22 +1200

workrave (1.8.4-8) unstable; urgency=low

  * Build with dbus support and provide an example script in
    /usr/share/doc/workrave/examples (closes: #373289)
  * Add a blurb in README.Debian about the above example script

 -- Francois Marier <francois@debian.org>  Mon, 27 Aug 2007 12:29:07 +1200

workrave (1.8.4-7) unstable; urgency=low

  * Suggest values for the micro and rest breaks in README.Debian
  * debian/control: mention the collab-maint svn repo

 -- Francois Marier <francois@debian.org>  Mon, 20 Aug 2007 14:57:37 +1200

workrave (1.8.4-6) unstable; urgency=low

  * Get a fix from upstream svn (closes: #419100)
  * Fixed the watch file
  * Reformat README.Debian, update the bugzilla link

 -- Francois Marier <francois@debian.org>  Mon, 13 Aug 2007 16:30:05 +1200

workrave (1.8.4-5) unstable; urgency=low

  * New maintainer (closes: #434492).  Thanks Michael for your work!
  * debian/rules: remove commented-out calls to dh_*
  * debian/copyright: be more explicit with the GPL and refer to
    /usr/share/common-licenses/GPL-2 instead of the GPL symlink
  * debian/control: expand RSI in the short description
  * debian/menu: change section for the menu transition
  * Sync with Ubuntu:
    - mutex crash: http://issues.workrave.org/cgi-bin/bugzilla/show_bug.cgi?id=611
    - POTFILES.in fixes

 -- Francois Marier <francois@debian.org>  Thu, 26 Jul 2007 22:24:06 +1200

workrave (1.8.4-4) unstable; urgency=high

  * Applied fix from upstream that supposedly fixes the crashes on startup
    (closes: 421366); I cannot check myself, but confirmed by others.
  * The bug closed has severity 'grave', therefore high urgency.

 -- Michael Piefel <piefel@debian.org>  Mon, 02 Jul 2007 10:42:06 +0200

workrave (1.8.4-3) unstable; urgency=low

  * Added a comment on how to fix a Workrave that disappeared when no panel at
    all is used (closes: #425359)

 -- Michael Piefel <piefel@debian.org>  Tue, 26 Jun 2007 11:59:39 +0200

workrave (1.8.4-2) unstable; urgency=low

  * Fix C++ includes (closes: #417762) thanks to Martin Michlmayr

 -- Michael Piefel <piefel@debian.org>  Thu, 10 May 2007 10:57:30 +0200

workrave (1.8.4-1) unstable; urgency=low

  * New upstream version
    - KDE applet space fixed (closes: #289382)
    - historystats reader fixed (closes: #390495)
  * Add dependency on libxtst-dev to enable XRecord extension (closes: #388669)

 -- Michael Piefel <piefel@debian.org>  Tue, 13 Mar 2007 10:36:47 +0100

workrave (1.8.3-1) unstable; urgency=low

  * New upstream version
    - builds with G++ 4.2 (closes: #357748)
    - breaks cleanly while screen is locked (closes: #334972)
    - shouldn’t crash anymore (or less regularly, closes: #365270)
    - applet popup menu is now accessible when all timers hidden (closes: #359931)
    - fixed Czech translation (closes: #365269)
  * New Brazilian Portuguese translation straight from Workrave issue tracker
    (closes: #320639 as well, I should hope)

 -- Michael Piefel <piefel@debian.org>  Mon, 15 May 2006 15:40:59 +0200

workrave (1.8.2-1) unstable; urgency=low

  * New upstream (closes: #353950)

 -- Michael Piefel <piefel@debian.org>  Fri, 24 Feb 2006 09:21:00 +0100

workrave (1.8.1-1) unstable; urgency=low

  * New upstream (closes: #321450)
  * New build closes: #271193, #290656
  * German PO fix (closes: #313885)

 -- Michael Piefel <piefel@debian.org>  Mon, 15 Aug 2005 11:59:49 +0200

workrave (1.6.2-1) unstable; urgency=low

  * New upstream. This supposedly closes: #251070; I hope it does.

 -- Michael Piefel <piefel@debian.org>  Tue, 03 Aug 2004 14:29:40 +0200

workrave (1.6.1-2) unstable; urgency=low

  * Extended build-deps.

 -- Michael Piefel <piefel@debian.org>  Fri, 21 May 2004 15:55:09 +0200

workrave (1.6.1-1) unstable; urgency=low

  * New upstream version. Unfortunate timing.

 -- Michael Piefel <piefel@debian.org>  Fri, 14 May 2004 11:14:59 +0200

workrave (1.6.0-2) unstable; urgency=low

  * As intltool requires Perl's XML parser, now build-depend on
    libxml-parser-perl (closes: #248650)

 -- Michael Piefel <piefel@debian.org>  Thu, 13 May 2004 16:59:58 +0200

workrave (1.6.0-1) unstable; urgency=low

  * New upstream version

 -- Michael Piefel <piefel@debian.org>  Wed, 12 May 2004 15:26:49 +0200

workrave (1.4.1-1) unstable; urgency=low

  * „Neues Orbit, neues Glück!“
  * New upstream version
  * This recompile might close #217998

 -- Michael Piefel <piefel@debian.org>  Wed, 05 Nov 2003 12:44:42 +0100

workrave (1.4.0-1) unstable; urgency=low

  * New upstream version (closes: #207723)
  * Add auto-update for config.*

 -- Michael Piefel <piefel@debian.org>  Fri, 29 Aug 2003 15:46:27 +0200

workrave (1.2.2.cvs20030617-3) unstable; urgency=low

  * Recompile with new orbit, this also closes: #198738

 -- Michael Piefel <piefel@debian.org>  Fri, 27 Jun 2003 13:49:23 +0200

workrave (1.2.2.cvs20030617-2) unstable; urgency=low

  * Make non-executable configure executable (closes: #198094)
    (hack, real fix when real new version arrives)
  * Add Build-Dep for g++-3.2, as g++-3.3 is too buggy for orbit

 -- Michael Piefel <piefel@debian.org>  Fri, 20 Jun 2003 09:07:20 +0200

workrave (1.2.2.cvs20030617-1) unstable; urgency=low

  * CVS checkout
  * Fixes sheep-when-respawning and mouse drift, I hope, and closes bugs
    189950 and 190802
  * Should also be buildable everywhere, I hope, which should close bugs
    189954 and 193426
  * Fix description typo (closes: #196661)
  * Rebuild makes it real sid again (closes: #192594)

 -- Michael Piefel <piefel@debian.org>  Tue, 17 Apr 2003 11:13:26 +0200

workrave (1.2.2-1) unstable; urgency=low

  * Changed section to gnome
  * New description from last version closes: #188233

 -- Michael Piefel <piefel@debian.org>  Tue, 15 Apr 2003 15:13:26 +0200

workrave (1.2.2-0) unstable; urgency=low

  * New upstream release
  * Added libgnomeuimm1.3-dev to build-depends, removed other
    build-depends that are implied by that

 -- Johannes Rohr <j.rohr@comlink.apc.org>  Tue, 25 Mar 2003 23:12:32 +0100

workrave (1.2.1-1) unstable; urgency=low

  * New upstream release (closes: #184205)
  * Added menu entry
  * Tightened up build-depends, added libpanel-applet2-dev
  * Revised extended package description to sound more attractive
  * Replaced debian/rules with version from upstream's CVS, in order to
    enable all the nice stuff that has been added to Workrave in version
    1.2 and to fix the typo that prevented the network capabilities from
    being compiled in
  * Removed call to dh_undocumented from debian/rules
  * Bumped up standards-version to 3.5.9

 -- Johannes Rohr <j.rohr@comlink.apc.org>  Mon, 24 Mar 2003 23:02:01 +0100

workrave (1.0.0-2) unstable; urgency=low

  * First official Debian package (closes: #162159)

 -- Michael Piefel <piefel@debian.org>  Tue, 25 Feb 2003 15:59:37 +0100

workrave (1.0.0-1) unstable; urgency=low

  * New upstream release

 -- Rob Caelers <robc@krandor.org>  Wed, 30 Oct 2002 20:02:40 +0100

workrave (0.3.0-1) unstable; urgency=low

  * New upstream release

 -- Rob Caelers <robc@krandor.org>  Fri, 11 Oct 2002 22:50:20 +0200

workrave (0.2.0-1) unstable; urgency=low

  * New upstream release

 -- Rob Caelers <robc@krandor.org>  Thu, 26 Sep 2002 19:50:52 +0200

workrave (0.1.0-1) unstable; urgency=low

  * First public release

 -- Rob Caelers <robc@krandor.org>  Sun,  22 Sep 2002 15:00:06 +0200
